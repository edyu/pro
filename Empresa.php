<?php
	require_once "IPais.php";
	require_once "Pais.php";
	require_once "Paises/Colombia.php";
	require_once "Paises/Japon.php";
	require_once "Paises/España.php";
	require_once "Paises/Canada.php";
	require_once "Paises/Argentina.php";

	
	class Empresa
	{
		public $Pais = null;
		public $resultado;
		public $retorno;

		
		public function ObtenerTotal(){
			$this->Calcular();
			$this->retorno = array(
				'precio' => $this->Pais->precio,
				'costo_peso' => $this->Pais->costo_peso,
				'precioextra' => $this->Pais->precioextra,
				'resultado' => $this->resultado
			);
			return $this->retorno;
		}

		public function Calcular()
		{
			$this->resultado = $this->Pais->precio + $this->Pais->precioextra;
		}

		public function DeterminarPais($Pais, $peso) {
			switch ($Pais) {
				case 'Colombia':
					$this->Pais = new Colombia($peso);
					break;
				case 'Argentina':
					$this->Pais = new Argentina($peso);
					break;
				case 'Canada':
					$this->Pais = new Canada($peso);
					break;											
				case 'España':
					$this->Pais = new España($peso);
					break;
				case 'Japon':
					$this->Pais = new Japon($peso);
					break;
				default :
					echo "Aun no estamos en dicho pais";
					break;
			}
		}
	}
?>
