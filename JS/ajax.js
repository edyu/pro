const calcular = async (event) => {
	event.preventDefault();
	const form = document.getElementById('frmPais');
	let jsonData = {};

	for (const pair of new FormData(form)) {
		jsonData[pair[0]] = pair[1];
	}
	const moneda = new Map([
		['Canada', '$'],
		['España', '€'],
		['Japon', '¥'],
		['Colombia', '$'],
		['Argentina', '$'],
	]);

	const form_d = new FormData(form);
	const pais = moneda.get(form_d.get('Pais'));

	const respuesta = await enviarPeticion('controller.php', jsonData);
	document.getElementById('precio').value = `${respuesta.body.precio} ${pais}`
	document.getElementById('costo_peso').value = `${respuesta.body.costo_peso} ${pais}`
	document.getElementById('precioextra').value = `${respuesta.body.precioextra} ${pais}`
	document.getElementById('resultado').value = `${respuesta.body.resultado} ${pais}`
}

const enviarPeticion = (action, jsonBody) => {
	const URL = `${document.location}/${action}`;
	return new Promise(async (resolve, reject) => {
		const rawResponse = await fetch(URL, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(jsonBody)
		})
			.then(response => response.text().then(data => {
				return ({ status: response.status, body: JSON.parse(data) })
			}
			))
			.then((data) => resolve(data))
			.catch(err => reject(Error(err.message)));
	});
};
