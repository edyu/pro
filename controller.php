<?php
	include_once "Empresa.php";
	
	$content = trim(file_get_contents("php://input"));
	$decoded = json_decode($content, true);

	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	header('Content-Type: application/json');

	try {
		calcular($decoded);
	} catch (Exception $e) {
		$return = array(
			'resultado' => $e->getMessage()
		);
		http_response_code(501);
		echo(json_encode($return));		
	}

	function calcular($decoded) {
		if(!is_array($decoded)){
			throw new Exception('Received content contained invalid JSON!');
		}

		$Pais= $decoded["Pais"];
		$peso= $decoded["Peso"];
		
		$Empresa = new Empresa();
		$Empresa->DeterminarPais($Pais, $peso);
		$retorno = $Empresa->ObtenerTotal();
		
		http_response_code(200);
		echo json_encode($retorno);		
	}
?>
