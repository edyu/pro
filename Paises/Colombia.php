<?php 
	include_once "IPais.php";
	include_once "Empresa.php";
	include_once "Pais.php";

	class Colombia extends Pais
	{

		public $impuesto = 0.12;
		public $precio = 2000;
		public $precioextra;
		public $resultado;
		public $peso;
		public $costo_peso;
		public $peso_control = 2;
		public $extra;
		
		public function __construct($peso)
		{
			$this->incremento($peso);
		}

		private function incremento($peso)
		{
			$this->peso = $peso;
			if ($peso < 2)
			{
				$this->extra = 750;
				$this->costo_peso = $this->peso * 750; 
				$this->precioextra = $this->precio * $this->impuesto;
			} else {
				$this->extra = 500;
				$this->costo_peso = $this->peso * 500;
				$this->precioextra = $this->precio * $this->impuesto;
			}
		}

	}
 ?>