<?php 
	include_once "IPais.php";
	include_once "Empresa.php";
	include_once "Pais.php";

	class Canada extends Pais
	{

		public $precio = 150;
		public $precioextra;
		public $resultado;
		public $peso = 0;
		public $costo_peso;
		public $peso_control = 4;

		public function __construct($peso)
		{
			$this->peso = $peso;
			$this->incremento($this->peso);
		}

		private function incremento($peso)
		{
			$this->peso = $peso;
			if ($this->peso < 4)
			{
				$this->extra=25;
				$this->costo_peso = $this->peso*25;
			}
			else
			{
				$this->costo_peso = $this->peso*20;
			}
		}
	}
 ?>