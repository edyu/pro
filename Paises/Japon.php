<?php 
	include_once "IPais.php";
	include_once "Empresa.php";
	include_once "Pais.php";

	class Japon extends Pais
	{

		public $impuesto = 0.11;
		public $precio = 200;
		public $precioextra;
		public $peso = 0;
		public $resultado;
		public $costo_peso;
		public $peso_control = 2;
		public $extra;

		public function __construct($peso)
		{
			$this->incremento($peso);
		}

		private function incremento($peso)
		{
			$this->peso = $peso;
			if ($peso < 2)
			{
				$this->extra = 25;
				$this->peso = $this->peso * 25; 
				$this->pesoextra = $this->precio*$this->impuesto;
			} else {
				$this->extra = 10;
				$this->peso = $this->$peso * 10;
				$this->pesoextra = $this->precio * $this->impuesto;
			}
		}
	}
 ?>