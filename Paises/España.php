<?php 
	include_once "IPais.php";
	include_once "Empresa.php";
	include_once "Pais.php";

	class España  extends Pais
	{

		public $impuesto = 0.7;
		public $precio = 150;
		public $precioextra;
		public $resultado;
		public $peso = 0;
		public $costo_peso;
		public $extra;

		public function __construct($peso)
		{
			$this->incremento($peso);
		}

		private function incremento($peso)
		{
			$this->peso = $peso;
			$this->extra = 10;
			$this->peso = $this->peso * 10;
			$this->precioextra = $this->peso * $this->impuesto;
		}

	}
 ?>