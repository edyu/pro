<?php 
	include_once "IPais.php";
	include_once "Empresa.php";
	include_once "Pais.php";

	class Argentina extends Pais
	{

		public $impuesto = 0.7;
		public $precio = 1500;
		public $precioextra;
		public $resultado;
		public $peso = 0;
		public $peso_control = 6;
		public $costo_peso;
		public $extra;

		public function __construct($peso)
		{
			$this->incremento($peso);
		}
		private function incremento($peso)
		{
			$this->peso = $peso;
			if ($this->peso < 6)
			{
				$this->extra = 350;
				$this->costo_peso = $this->peso * 350; 
				$this->pesoextra = $this->precio*$this->impuesto;
			}
			else
			{
				$this->extra = 100;
				$this->costo_peso = $this->peso*100;
				$this->pesoextra = $this->precio*$this->impuesto;
			}
		}

	}
 ?>